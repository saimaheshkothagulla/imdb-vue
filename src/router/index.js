import Vue from 'vue'
import VueRouter from 'vue-router'
import ListMovies from '../views/ListMovies.vue'
import Actors from '../views/Actors.vue'
import Producers from '../views/Producers.vue'
import AddMovie from '../views/AddMovie.vue'
import EditMovie from '../views/EditMovie.vue'

Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'ListMovies',
        component: ListMovies
    },
    {
        path: '/actors',
        name: 'Actors',
        component: Actors
    },
    {
        path: '/producers',
        name: 'Producers',
        component: Producers
    },
    {
        path: '/addmovie',
        name: 'AddMovie',
        component: AddMovie
    },
    {
        path: '/editmovie',
        name: 'EditMovie',
        component: EditMovie,
        props: true
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router