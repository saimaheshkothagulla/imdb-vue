import axios from 'axios'

export default {
    getProducers: async() => {
        try {
            let response = await axios.get("https://localhost:44330/producers")
            return response.data
        } catch {
            return 0
        }
    },
    addProducer: async(producer) => {
        try {
            let stringifyProducer = JSON.stringify(producer)
            await axios.post("https://localhost:44330/producers", stringifyProducer)
            return true
        } catch {
            return false
        }
    }
}