import axios from 'axios'

export default {
    getActors: async() => {
        try {
            let response = await axios.get("https://localhost:44330/actors")
            return response.data
        } catch {
            return 0
        }
    },
    addActor: async(actor) => {
        try {
            let stringifyActor = JSON.stringify(actor)
            await axios.post("https://localhost:44330/actors", stringifyActor)
            return true
        } catch {
            return false
        }
    }
}