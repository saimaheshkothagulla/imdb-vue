import axios from 'axios'

export default {
    getGenres: async() => {
        try {
            const response = await axios.get("https://localhost:44330/genres")
            return response.data
        } catch {
            return 0
        }
    }
}