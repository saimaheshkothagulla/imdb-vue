import axios from 'axios'

export default {
    getMovies: async() => {
        try {
            const response = await axios.get("https://localhost:44330/movies")
            return response.data
        } catch {
            return 0
        }
    },
    deleteMovie: async(id) => {
        try {
            let url = "https://localhost:44330/movies/" + id;
            let options = {
                url: url,
                method: 'delete'
            }
            axios.delete(url, options)
            return true
        } catch {
            return false
        }
    },
    addMovie: async(movie) => {
        try {
            let stringifyMovie = JSON.stringify(movie)
            await axios.post("https://localhost:44330/movies", stringifyMovie)
            return true
        } catch {
            return false
        }

    },
    editMovie: async(movie, id) => {
        try {
            let url = "https://localhost:44330/movies/" + id
            let stringifyMovie = JSON.stringify(movie)
            await axios.put(url, stringifyMovie)
            return true
        } catch {
            return false
        }

    }
}